package com.howard.weatherchanneltv.ui.theme

import androidx.compose.ui.unit.dp
import androidx.tv.material3.ExperimentalTvMaterial3Api
import androidx.tv.material3.ShapeDefaults

@OptIn(ExperimentalTvMaterial3Api::class)
val WeatherChannelCardShape = ShapeDefaults.ExtraSmall

@OptIn(ExperimentalTvMaterial3Api::class)
val WeatherChannelButtonShape = ShapeDefaults.ExtraSmall
val IconSize = 20.dp
val WeatherChannelBorderWidth = 3.dp

/**
 * Space to be given below every Lazy (or scrollable) vertical list throughout the app
 */
val WeatherChannelBottomListPadding = 28.dp