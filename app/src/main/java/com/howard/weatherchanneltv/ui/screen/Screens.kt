package com.howard.weatherchanneltv.ui.screen

import androidx.compose.ui.graphics.vector.ImageVector
import com.howard.weatherchanneltv.ui.screen.categories.screen.CategoryMovieListScreen
import com.howard.weatherchanneltv.ui.screen.movies.screen.MovieDetailsScreen
import com.howard.weatherchanneltv.ui.screen.videoplayer.screen.VideoPlayerScreen

enum class Screens(
    private val args: List<String>? = null,
    val isTabItem: Boolean = false,
    val tabIcon: ImageVector? = null
) {
    Profile,
    Home(isTabItem = true),
    Categories(isTabItem = true),
    Movies(isTabItem = true),
    CategoryMovieList(listOf(CategoryMovieListScreen.CategoryIdBundleKey)),
    MovieDetails(listOf(MovieDetailsScreen.MovieIdBundleKey)),
    Dashboard,
    VideoPlayer(listOf(VideoPlayerScreen.MovieIdBundleKey));

    operator fun invoke(): String {
        val argList = StringBuilder()
        args?.let { nnArgs ->
            nnArgs.forEach { arg -> argList.append("/{$arg}") }
        }
        return name + argList
    }

    fun withArgs(vararg args: Any): String {
        val destination = StringBuilder()
        args.forEach { arg -> destination.append("/$arg") }
        return name + destination
    }
}