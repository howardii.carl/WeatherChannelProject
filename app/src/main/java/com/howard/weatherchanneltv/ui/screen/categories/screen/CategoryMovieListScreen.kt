package com.howard.weatherchanneltv.ui.screen.categories.screen

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel

import androidx.tv.foundation.lazy.grid.TvGridCells
import androidx.tv.foundation.lazy.grid.TvGridItemSpan
import androidx.tv.foundation.lazy.grid.TvLazyVerticalGrid
import androidx.tv.material3.Border
import androidx.tv.material3.CardDefaults
import androidx.tv.material3.CardLayoutDefaults
import androidx.tv.material3.ExperimentalTvMaterial3Api
import androidx.tv.material3.MaterialTheme
import androidx.tv.material3.StandardCardLayout
import androidx.tv.material3.Text
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.howard.weatherchanneltv.data.entities.Movie
import com.howard.weatherchanneltv.data.entities.MovieCategoryDetails
import com.howard.weatherchanneltv.data.util.StringConstants
import com.howard.weatherchanneltv.ui.screen.categories.viewmodel.CategoryMovieListScreenUiState
import com.howard.weatherchanneltv.ui.screen.categories.viewmodel.CategoryMovieListScreenViewModel
import com.howard.weatherchanneltv.ui.screen.dashboard.screen.rememberChildPadding
import com.howard.weatherchanneltv.ui.theme.WeatherChannelBorderWidth
import com.howard.weatherchanneltv.ui.theme.WeatherChannelBottomListPadding
import com.howard.weatherchanneltv.ui.theme.WeatherChannelCardShape
import com.howard.weatherchanneltv.ui.utils.focusOnInitialVisibility

object CategoryMovieListScreen {
    const val CategoryIdBundleKey = "categoryId"
}

@Composable
fun CategoryMovieListScreen(
    onBackPressed: () -> Unit,
    onMovieSelected: (Movie) -> Unit,
    categoryMovieListScreenViewModel: CategoryMovieListScreenViewModel = hiltViewModel()
) {
    val uiState by categoryMovieListScreenViewModel.uiState.collectAsState()

    when (val s = uiState) {
        is CategoryMovieListScreenUiState.Loading -> {
            Loading()
        }

        is CategoryMovieListScreenUiState.Error -> {
            Error()
        }

        is CategoryMovieListScreenUiState.Done -> {
            val categoryDetails = s.movieCategoryDetails
            CategoryDetails(
                categoryDetails = categoryDetails,
                onBackPressed = onBackPressed,
                onMovieSelected = onMovieSelected
            )
        }
    }

}

@OptIn(ExperimentalTvMaterial3Api::class)
@Composable
private fun CategoryDetails(
    categoryDetails: MovieCategoryDetails,
    onBackPressed: () -> Unit,
    onMovieSelected: (Movie) -> Unit,
    modifier: Modifier = Modifier
) {
    val childPadding = rememberChildPadding()
    val isFirstItemVisible = remember { mutableStateOf(false) }

    BackHandler(onBack = onBackPressed)

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier,
    ) {
        Text(
            text = categoryDetails.name,
            style = MaterialTheme.typography.displaySmall.copy(
                fontWeight = FontWeight.SemiBold
            ),
            modifier = Modifier.padding(
                vertical = childPadding.top.times(3.5f)
            )
        )
        TvLazyVerticalGrid(
            columns = TvGridCells.Fixed(6)
        ) {
            categoryDetails.movies.forEachIndexed { index, movie ->
                item {
                    key(movie.id) {
                        StandardCardLayout(
                            modifier = Modifier
                                .aspectRatio(1 / 1.5f)
                                .padding(8.dp)
                                .then(
                                    if (index == 0)
                                        Modifier.focusOnInitialVisibility(isFirstItemVisible)
                                    else Modifier
                                ),
                            imageCard = {
                                CardLayoutDefaults.ImageCard(
                                    shape = CardDefaults.shape(shape = WeatherChannelCardShape),
                                    border = CardDefaults.border(
                                        focusedBorder = Border(
                                            border = BorderStroke(
                                                width = WeatherChannelBorderWidth,
                                                color = MaterialTheme.colorScheme.onSurface
                                            ),
                                            shape = WeatherChannelCardShape
                                        ),
                                        pressedBorder = Border(
                                            border = BorderStroke(
                                                width = WeatherChannelBorderWidth,
                                                color = MaterialTheme.colorScheme.border
                                            ),
                                            shape = WeatherChannelCardShape
                                        ),
                                    ),
                                    scale = CardDefaults.scale(focusedScale = 1f),
                                    onClick = { onMovieSelected(movie) },
                                    interactionSource = it
                                ) {
                                    AsyncImage(
                                        model = ImageRequest.Builder(LocalContext.current)
                                            .data(movie.posterUri)
                                            .crossfade(true)
                                            .build(),
                                        contentDescription = StringConstants
                                            .Composable
                                            .ContentDescription
                                            .moviePoster(movie.name),
                                        contentScale = ContentScale.Crop,
                                        modifier = Modifier.fillMaxSize()
                                    )
                                }
                            },
                            title = {},
                        )
                    }
                }
            }
            item(span = { TvGridItemSpan(currentLineSpan = 6) }) {
                Spacer(modifier = Modifier.padding(bottom = WeatherChannelBottomListPadding))
            }
        }
    }
}

@OptIn(ExperimentalTvMaterial3Api::class)
@Composable
private fun Loading(modifier: Modifier = Modifier) {
    Text(text = "Loading...", modifier = modifier)
}

@OptIn(ExperimentalTvMaterial3Api::class)
@Composable
private fun Error(modifier: Modifier = Modifier) {
    Text(text = "Wops, something went wrong...", modifier = modifier)
}