package com.howard.weatherchanneltv.network

import com.google.android.exoplayer2.MediaItem
import retrofit2.http.Path

interface ApiService {
    //Todo @Get('data')
    suspend fun getData(): List<MediaItem>

    //Todo @Get('id')
    suspend fun getDetails(@Path("id") id: String): MediaItem
}