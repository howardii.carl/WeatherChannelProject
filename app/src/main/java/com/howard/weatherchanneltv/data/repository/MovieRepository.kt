package com.howard.weatherchanneltv.data.repository

import com.howard.weatherchanneltv.data.entities.MovieCategoryDetails
import com.howard.weatherchanneltv.data.entities.MovieCategoryList
import com.howard.weatherchanneltv.data.entities.MovieDetails
import com.howard.weatherchanneltv.data.entities.MovieList
import kotlinx.coroutines.flow.Flow

interface MovieRepository {
    fun getFeaturedMovies(): Flow<MovieList>
    fun getTrendingMovies(): Flow<MovieList>
    fun getTop10Movies(): Flow<MovieList>
    fun getNowPlayingMovies(): Flow<MovieList>
    fun getMovieCategories(): Flow<MovieCategoryList>
    suspend fun getMovieCategoryDetails(categoryId: String): MovieCategoryDetails
    suspend fun getMovieDetails(movieId: String): MovieDetails
    fun getMoviesWithLongThumbnail(): Flow<MovieList>
    fun getMovies(): Flow<MovieList>
    fun getPopularFilmsThisWeek(): Flow<MovieList>
    fun getBingeWatchDramas(): Flow<MovieList>
}
