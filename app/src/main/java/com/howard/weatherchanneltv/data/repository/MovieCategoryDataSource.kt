package com.howard.weatherchanneltv.data.repository

import com.howard.weatherchanneltv.data.entities.toMovieCategory
import com.howard.weatherchanneltv.data.util.AssetsReader
import com.howard.weatherchanneltv.data.util.StringConstants
import javax.inject.Inject

class MovieCategoryDataSource @Inject constructor(
    assetsReader: AssetsReader
) {

    private val movieCategoryDataReader = CachedDataReader {
        readMovieCategoryData(assetsReader, StringConstants.Assets.MovieCategories).map {
            it.toMovieCategory()
        }
    }

    suspend fun getMovieCategoryList() = movieCategoryDataReader.read()

}