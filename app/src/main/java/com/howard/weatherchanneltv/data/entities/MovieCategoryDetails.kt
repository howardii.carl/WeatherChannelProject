package com.howard.weatherchanneltv.data.entities

data class MovieCategoryDetails(
    val id: String,
    val name: String,
    val movies: MovieList,
)
