package com.howard.weatherchanneltv.data.repository

import com.howard.weatherchanneltv.data.entities.toMovieCast
import com.howard.weatherchanneltv.data.util.AssetsReader
import com.howard.weatherchanneltv.data.util.StringConstants
import javax.inject.Inject

class MovieCastDataSource @Inject constructor(
    assetsReader: AssetsReader
) {

    private val movieCastDataReader = CachedDataReader {
        readMovieCastData(assetsReader, StringConstants.Assets.MovieCast).map {
            it.toMovieCast()
        }
    }

    suspend fun getMovieCastList() = movieCastDataReader.read()


}