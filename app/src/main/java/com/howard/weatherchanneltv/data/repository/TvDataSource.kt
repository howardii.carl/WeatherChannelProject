package com.howard.weatherchanneltv.data.repository

import com.howard.weatherchanneltv.data.entities.toMovie
import com.howard.weatherchanneltv.data.util.AssetsReader
import com.howard.weatherchanneltv.data.util.StringConstants
import javax.inject.Inject

class TvDataSource @Inject constructor(
    assetsReader: AssetsReader
) {
    private val mostPopularTvShowsReader = CachedDataReader {
        readMovieData(assetsReader, StringConstants.Assets.MostPopularTVShows)
    }

    suspend fun getBingeWatchDramaList() = mostPopularTvShowsReader.read().subList(6, 15).map {
        it.toMovie()
    }

}