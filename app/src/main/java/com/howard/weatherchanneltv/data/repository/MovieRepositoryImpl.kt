package com.howard.weatherchanneltv.data.repository

import com.howard.weatherchanneltv.data.entities.MovieCategoryDetails
import com.howard.weatherchanneltv.data.entities.MovieCategoryList
import com.howard.weatherchanneltv.data.entities.MovieDetails
import com.howard.weatherchanneltv.data.entities.MovieList
import com.howard.weatherchanneltv.data.entities.MovieReviewsAndRatings
import com.howard.weatherchanneltv.data.entities.ThumbnailType
import com.howard.weatherchanneltv.data.util.StringConstants
import com.howard.weatherchanneltv.data.util.StringConstants.Movie.Reviewer.DefaultCount
import com.howard.weatherchanneltv.data.util.StringConstants.Movie.Reviewer.DefaultRating
import com.howard.weatherchanneltv.data.util.StringConstants.Movie.Reviewer.FreshTomatoes
import com.howard.weatherchanneltv.data.util.StringConstants.Movie.Reviewer.ReviewerName
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieRepositoryImpl @Inject constructor(
    private val movieDataSource: MovieDataSource,
    private val tvDataSource: TvDataSource,
    private val movieCastDataSource: MovieCastDataSource,
    private val movieCategoryDataSource: MovieCategoryDataSource,
) : MovieRepository {

    override fun getFeaturedMovies() = flow {
        val list = MovieList(movieDataSource.getFeaturedMovieList())
        emit(list)
    }

    override fun getTrendingMovies(): Flow<MovieList> = flow {
        val list = MovieList(movieDataSource.getTrendingMovieList())
        emit(list)
    }

    override fun getTop10Movies(): Flow<MovieList> = flow {
        val list = MovieList(movieDataSource.getTop10MovieList())
        emit(list)
    }

    override fun getNowPlayingMovies(): Flow<MovieList> = flow {
        val list = MovieList(movieDataSource.getNowPlayingMovieList())
        emit(list)
    }

    override fun getMovieCategories() = flow {
        val list = MovieCategoryList(movieCategoryDataSource.getMovieCategoryList())
        emit(list)
    }

    override suspend fun getMovieCategoryDetails(categoryId: String): MovieCategoryDetails {
        val categoryList = movieCategoryDataSource.getMovieCategoryList()
        val category = categoryList.find { categoryId == it.id } ?: categoryList.first()

        val movieList = movieDataSource.getMovieList().shuffled().subList(0, 20)

        return MovieCategoryDetails(
            id = category.id,
            name = category.name,
            movies = MovieList(movieList)
        )
    }

    override suspend fun getMovieDetails(movieId: String): MovieDetails {
        val movieList = movieDataSource.getMovieList()
        val movie = movieList.find { it.id == movieId } ?: movieList.first()
        val similarMovieList = movieList.shuffled().subList(0, 2)
        val castList = movieCastDataSource.getMovieCastList()

        return MovieDetails(
            id = movie.id,
            videoUri = movie.videoUri,
            subtitleUri = movie.subtitleUri,
            posterUri = movie.posterUri,
            name = movie.name,
            description = movie.description,
            pgRating = "PG-13",
            releaseDate = "2021 (US)",
            categories = listOf("Action", "Adventure", "Fantasy", "Comedy"),
            duration = "1h 59m",
            director = "Larry Page",
            screenplay = "Sundai Pichai",
            music = "Sergey Brin",
            castAndCrew = castList,
            status = "Released",
            originalLanguage = "English",
            budget = "$15M",
            revenue = "$40M",
            similarMovies = MovieList(similarMovieList),
            reviewsAndRatings = listOf(
                MovieReviewsAndRatings(
                    reviewerName = FreshTomatoes,
                    reviewerIconUri = StringConstants.Movie.Reviewer.FreshTomatoesImageUrl,
                    reviewCount = "22",
                    reviewRating = "89%"
                ),
                MovieReviewsAndRatings(
                    reviewerName = ReviewerName,
                    reviewerIconUri = StringConstants.Movie.Reviewer.ImageUrl,
                    reviewCount = DefaultCount,
                    reviewRating = DefaultRating
                ),
            )
        )
    }


    override fun getMoviesWithLongThumbnail() = flow {
        val list = movieDataSource.getMovieList(ThumbnailType.Long)
        emit(MovieList(list))
    }

    override fun getMovies(): Flow<MovieList> = flow {
        val list = movieDataSource.getMovieList()
        emit(MovieList(list))
    }

    override fun getPopularFilmsThisWeek(): Flow<MovieList> = flow {
        val list = movieDataSource.getPopularFilmThisWeek()
        emit(MovieList(list))
    }


    override fun getBingeWatchDramas(): Flow<MovieList> = flow {
        val list = tvDataSource.getBingeWatchDramaList()
        emit(MovieList(list))
    }


}