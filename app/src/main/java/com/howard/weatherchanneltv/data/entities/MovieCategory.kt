package com.howard.weatherchanneltv.data.entities

import com.howard.weatherchanneltv.data.model.MovieCategoriesResponseItem

data class MovieCategory(
    val id: String,
    val name: String,
)

fun MovieCategoriesResponseItem.toMovieCategory(): MovieCategory =
    MovieCategory(id, name)